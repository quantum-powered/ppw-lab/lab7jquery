from django.shortcuts import render, redirect
from landingPage import views
from django.contrib.auth import authenticate, login

# Create your views here.
def login_index (request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect("/")

    return render(request, 'login/login.html')
