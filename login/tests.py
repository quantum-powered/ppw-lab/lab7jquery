from django.test import TestCase, Client
from django.urls import resolve
from login.views import login_index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from django.contrib.auth.models import User

from django.contrib.staticfiles.testing import StaticLiveServerTestCase

# Create your tests here.
class LoginExistTestcase(TestCase):
    def test_login_url_exist(self):
        respose = Client().get("/user/login/")
        self.assertEqual(respose.status_code, 200)

    def test_login_template(self):
        response = Client().get('/user/login/')
        self.assertTemplateUsed(response, 'login/login.html')

    def test_login_using_index_func(self):
        found = resolve('/user/login/')
        self.assertEqual(found.func, login_index) 

class LoginTestcase(TestCase):
    def test_can_login(self):
        user = User.objects.create_user('foo', 'myemail@test.com', 'bar')
        response = Client().post('/user/login/', {'username': 'foo', 'password': 'bar'})
        self.assertEqual(response.status_code, 302)

        self.assertFalse(response.wsgi_request.user.is_anonymous)
        self.assertEqual(response.wsgi_request.user, user)

    
    def test_notregistered_user_cant_login(self):
        user = User.objects.create_user('foo', 'myemail@test.com', 'bar')
        response = Client().post('/user/login/', {'username': 'bukan', 'password': 'bukan'})
        self.assertEqual(response.status_code, 200)

        self.assertTrue(response.wsgi_request.user.is_anonymous)

        
class LoginFunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')   
        chrome_options.add_argument('--headless')     

        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)
        
        super(LoginFunctionalTest, self).setUp()
        user = User.objects.create_user(username='testuser', password='12345')

    def tearDown(self):
        self.selenium.quit()
        super(LoginFunctionalTest, self).tearDown()

    def test_ajax_request_with_enter(self):
        selenium = self.selenium

        
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/user/login/')
        # find the form element

        username = selenium.find_element_by_id('username')
        username.send_keys('test')

        password = selenium.find_element_by_id('password')
        password.send_keys('kentanggoreng')

        button = selenium.find_element_by_name('login-button')
        button.send_keys(Keys.RETURN)

   