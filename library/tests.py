from django.test import TestCase, Client
from django.urls import resolve
from library.views import library_index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase

# Create your tests here.
class libraryExistTestcase(TestCase):
    def test_library_url_exist(self):
        respose = Client().get("/library/")
        self.assertEqual(respose.status_code, 200)

    def test_library_template(self):
        response = Client().get('/library/')
        self.assertTemplateUsed(response, 'library/library.html')

    def test_library_using_index_func(self):
        found = resolve('/library/')
        self.assertEqual(found.func, library_index) 

class LibraryFunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)
        super(LibraryFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(LibraryFunctionalTest, self).tearDown()

    def test_ajax_request_with_enter(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/library')
        # find the form element

        searchbox = selenium.find_element_by_id('search-book-box')
        searchbox.send_keys('harry potter')
        searchbox.send_keys(Keys.RETURN)

        time.sleep(1)
        selenium.find_element_by_xpath('/html/body/div[3]/div[1]')
    
    def test_ajax_request_with_button(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/library')
        # find the form element

        searchbox = selenium.find_element_by_id('search-book-box')
        searchbox.send_keys('harry potter')
        
        button = selenium.find_element_by_id('search-button')
        button.click()

        time.sleep(1)
        selenium.find_element_by_xpath('/html/body/div[3]/div[1]')



