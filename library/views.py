from django.shortcuts import render

# Create your views here.
def library_index(request):
    return render(request, 'library/library.html', {})