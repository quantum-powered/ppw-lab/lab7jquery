$(document).ready(function(){
    var hasil = $(".hasil").hide();

    $("#search-button").click(function(){
        var search = $("#search-book-box")
        
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + search.val(),
            dataType: "json",

            success: function(data) {
                $('.card').remove();
                console.log(data);
                for(i = 0; i < data.items.length; i++){
                    var card = "<div class='card m-2' style='width: 18rem;'>"
                    card += '<img src='+ data.items[i].volumeInfo.imageLinks.thumbnail+' class="card-img-top img-fluid text-center mx-auto my-2" id="img-buku"></img>'
                    card += '<div class="card-body p-3">'
                    card += "<p class='card-title h6 text-center font-weight-bold'>" + data.items[i].volumeInfo.title + "</>"
                    if (typeof data.items[i].volumeInfo.authors !== 'undefined'){
                        card += "<p class='card-body text-center font-weight-light p-2'>" + data.items[i].volumeInfo.authors
                        if (typeof data.items[i].volumeInfo.publishedDate !== 'undefined'){
                            card += " (" + data.items[i].volumeInfo.publishedDate + ")"
                        }
                        card += "</p>"   
                    }

                    card += "</div>"
                    card += "</div>"

                    $(".hasil").append(card);
                }
            },

            type: 'GET'
        });

        hasil.show();
    });

    $('#search-book-box').keyup(function(e){
        console.log(e.which)
        if(e.which == 13){//Enter key pressed
            $('#search-button').click();//Trigger search button click event
        }
    });
});