from django.urls import path
from landingPage import views

app_name = "landingPage"

urlpatterns = [
    path('', views.index, name='index'),
    path('add/', views.add, name='add'),
    path('about/', views.about, name='about'),
]