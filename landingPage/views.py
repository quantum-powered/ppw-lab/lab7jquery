from django.shortcuts import render, redirect
from landingPage.models import Status
from landingPage.forms import FormStatus


# Create your views here.
def index(request):
    query = {
        'form' : FormStatus,
        'data' : Status.objects.order_by('-timestamp')
    }
    return render(request, 'landingPage/index.html', context=query)

def add(request):
    if request.method == 'POST':
        form = FormStatus(request.POST)

        if form.is_valid():
            form.save(commit = True)

    return redirect('/')

def about(request):
    return render(request, 'landingPage/about-me.html', {})